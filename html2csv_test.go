package main

import (
	"bytes"
	"os"
	"testing"
)

func TestCreateCSVFromHTML(t *testing.T) {
	file, err := os.Open("test/testdata/Objektorientierte Programmierung 1 [INH02007UF] _s Participants.html")
	if err != nil {
		t.Errorf("err=%q\n", err)
	}
	defer file.Close()

	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(file)

	if err != nil {
		t.Errorf("err=%q\n", err)
	}

	html2csv, err := NewHTML2CSV("test/testdata/config.toml")
	if err != nil {
		t.Errorf("err=%q\n", err)
	}

	err = html2csv.CreateCSVFromHTML(buf.String(), "test/out.csv")
	if err != nil {
		t.Errorf("err=%q\n", err)
	}

}
