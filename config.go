package main

import (
	"fmt"
	"os"

	"github.com/BurntSushi/toml"
)

type Config struct {
	WebCrawler *WebCrawler
	HTML2CSV   *HTML2CSV
}

func ReadConfig(filename string) (*Config, error) {

	cfg := &Config{}

	if _, err := os.Stat(filename); os.IsNotExist(err) || err != nil {
		fmt.Fprintln(os.Stderr, err)
		return nil, err
	}

	_, err := toml.DecodeFile(filename, cfg)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return nil, err
	}

	return cfg, nil
}
